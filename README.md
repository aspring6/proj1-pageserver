# Proj1-Page Server
This project handles GET requests to a server, sending the according response back depending on if the page is present or not.

## Author: Alec Springel, aspring6@uoregon.edu ##

# Usage
To run the program, configure credentials.ini, and run:  
```make start``` and connect via a web browser using the port from crednetials.ini